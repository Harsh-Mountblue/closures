function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned
    let count = 0;
    return function limit(a, b) {
        if (count < n) {
            count +=1;
            console.log(cb(a, b));
            return cb(a, b);
        }
    }
}


module.exports = limitFunctionCallCount;