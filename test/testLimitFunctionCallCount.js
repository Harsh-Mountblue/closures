const temp = require('../limitFunctionCallCount.js');
// console.log(temp)

const call = temp((myFun = (a,b) => a+b),8);
a = 0;
b = 1;
for (let i = 0; i < 10; i++) {
    a = call(a, b);
    b = a - b;
}